@extends('layouts.master')
@section('title'.'Add')
@section('content')
<form action="{{url('people')}}" method="POST">
    @csrf
    
    <h1>Add People</h1><br>
    <div class="form-group">
        <label>Firatname</label>
        <input type="text" class="form-control" name="fname">
    </div>
    <div class="form-group">
            <label>Lastname</label>
            <input type="text" class="form-control" name="lname">
    </div>
    <div class="form-group">
            <label>Age</label>
            <input type="text" class="form-control" name="age">
    </div>
    <button type="submit" class="btn btn-primary">Save</button>
    <br><br>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
</form>
@endsection
