@extends('layouts.master')
@section('title'.'Edit')
@section('content')
<form action="{{url('people',[$people->id])}}" method="POST">
    @csrf
    @method('put')
    <h1>Edit People</h1>
    <div class="form-group">
        <label>Firatname</label>
    <input type="text" class="form-control" name="fname" value="{{$people->fname}}">
    </div>
    <div class="form-group">
            <label>Lastname</label>
    <input type="text" class="form-control" name="lname" value="{{$people->lname}}">
    </div>
    <div class="form-group">
            <label>Age</label>
    <input type="text" class="form-control" name="age" value="{{$people->age}}">
    </div>
    <button type="submit" class="btn btn-primary">Edit</button>
    <br><br>
    @if($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
    @endif
</form>
@endsection
