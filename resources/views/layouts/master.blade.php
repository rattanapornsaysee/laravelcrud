<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    
    @section('css')
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    @show
</head>
<body>
    {{-- if else แบบยาว --}}
    <?php 
        // if(isset($id)){
        //     echo $id;
        // }else {
        //     echo "Not Found";
        // }
    ?>
    {{-- แบบสั้นลงมานิด1 --}}

   {{-- @if(isset($id))
        {{$id}}
   @else
        <h1>Not Found</h1>
   @endif --}}

   {{-- แบบสั้น --}}

   {{-- {{ isset($id)? $id: 'Not Found' }} --}}
    
   {{-- สั้นมาก --}}

   {{-- {{$id ?? 'Not Found'}} --}}
   


   <?php 
//    for ($i=0; $i < 10; $i++) { 
//        echo $i;
//    }
?>
{{-- 
@for ($i = 0; $i < 5; $i++)
   <span>{{ $i }}</span>
@endfor

@foreach ([1,2,3,4] as $a)
   {{ $a }}
@endforeach --}}

{{-- @forelse ($collection as $item)
   @if ()
       
   @endif
@empty
   
@endforelse --}}

<div class="container mt-5">
    {{-- <h1 class="main-aqua">Bule</h1> --}}
    @include('layouts.navbar')
    @yield('content')
    @yield('js')
    @include('layouts.footer')
</div>
</body>
</html>